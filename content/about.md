+++
title = 'About'
date = 2023-10-03T12:51:37-03:00
draft = false
+++

Olá eu sou o Leonardo, um estudante de Engenharia Mecatrônica da Escola Politécnica. Fiz esse site para um disciplina da graduação (Sistemas de Informação) com o intuito de aprender e exercitar conhecimentos relacionados ao projeto de um site em HTML e CSS com Hugo.

Falando mais sobre mim sou nascido em Curitiba - PR e me mudei para São Paulo em 2022 para estudar na USP, aqui venho tendo uma vida nova na faculdade conhecendo muitas pessoa e me envolvendo em diversos projetos. Entre esses projetos dois grupos se destacaram sendo eles o Centro Acadêmico de Mecânica e Mecatrônica e o Projeto Júpiter, sendo o ultimo citado na aba de Hobbies desse site.

Nesses grupos acabei participando de projetos dês de a organização de uma festa para mais de 2 mil pessoas até o projeto, fabricação e lançamento de foguetes. Além de desenvolver diversas habilidades técnicas e sociais criei diversos laços e comunidades nesses grupos.

Enfim, poderia criar toda uma biografia aqui mas o intuito era mais explicar um pouco sobre o intuito de criar esse site e preencher um pouco de espaço para testar como o conteúdo ia se comportar. Obrigado por ter lido até aqui.