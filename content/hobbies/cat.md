+++
title = 'Gatos'
date = 2023-10-03T13:15:00-03:00
picture = "/images/cat.gif"
draft = false
+++

![Text](../images/cat.svg "Title")

Eu realmente gosto muito de gatos, fico vendo videos de gatos o dia todo.

Eu tinha dois gatos mas eles ficaram na casa dos meus pais em curitiba quando me mudei pra São Paulo porque eles ficariam muito sozinhos aqui comigo passando quase o dia todo na faculdade.

Mas ainda assim minha paixão interna por gatos prevalece.
