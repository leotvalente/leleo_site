+++
title = 'Foguete'
date = 2023-10-03T13:15:00-03:00
draft = false
+++

![Text](../images/rocket.svg "Title")

Foguetes sempre foram uma paixão minha, tanto que faço parte do Projeto Jupiter, um grupo de extensão da poli de foguetemodelismo universitário.

Lá atuo como gerente da area de propulsão, a area responsável por projetar a fabricar o motor dos foguetes do Projeto.