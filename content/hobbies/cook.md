+++
title = 'Cozinhar'
date = 2023-10-03T13:15:00-03:00
draft = false
+++

![Text](../images/cook.svg "Title")

Cozinhar não é apenas um hobby para mim, é a forma mais deliciosa de expressar minha criatividade e compartilhar amor em forma de comida.